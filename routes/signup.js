/**
 * Created by tarek on 9/8/15.
 */

var express = require('express');

var auth = require('../helpers/authentication');
var pass = require('../helpers/pass');
var user = require('../model/user');
var validation = require( '../helpers/validation')

var router = express.Router();

router.get("/", function (req, res) {
    if (req.session.user) {
        res.redirect("/");
    } else {
        res.render("signup", {error:''});
    }
});

router.post("/", auth.userExist, validation.checkUserBody, function (req, res) {
    var password = req.body.password;
    var email = req.body.email;
    var firstName = req.body.first_name;
    var lastName = req.body.last_name;

    pass.hash(password, function (err, salt, hash) {
        if (err) throw err;
        var userObject = {
            email: email,
            first_name:firstName,
            last_name:lastName,
            salt: salt,
            hash: hash,
            role: "user"
        };
        user.insert(userObject,function (err, newUser) {
                if (err) throw err;
                auth.authenticate(newUser.email, password, function(err, user){
                    if(user){
                        req.session.regenerate(function(){
                            req.session.user = user;
                            req.session.success = 'Authenticated as ' + user.username + ' click to <a href="/logout">logout</a>. ' + ' You may now access <a href="/restricted">/restricted</a>.';
                            res.redirect('/');
                        });
                    }
                });
            });
    });
});

module.exports = router;