/**
 * Created by tarek on 10/17/15.
 */

var express = require('express');

var movie = require('../model/movie');


var router = express.Router();

/* GET users listing. */
router.get('/:id', function (req, res, next) {
    var condition = { id: req.params.id};
    movie.findOne(condition,function (err, movie) {
        console.log(movie);
        res.render('movie', {
            movie: movie,
            user : req.session.user
        });
    });
});



module.exports = router;