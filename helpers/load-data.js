/**
 * Created by tarek on 10/15/15.
 */
var mongoose = require('mongoose');
var fs = require('fs');

mongoose.connect('mongodb://localhost/accedo');

var movieSchema = new mongoose.Schema({
    title: { type: String, required: true  },
    description: String,
    type: String,
    publishedDate: Date,
    availableDate: Date,
    metadata:[],
    contents:[],
    credits:[],
    parentalRatings:[],
    images:[],
    categories:[],
    id:{ type: String, required: true , unique: true }
});

var movie = mongoose.model('movies', movieSchema);
var movies = JSON.parse(fs.readFileSync('movies.json', 'utf8'));

for(var i = 0;i<movies.entries.length;i++){
    var movie_entry = new movie(movies.entries[i]);
    movie_entry.save(function(err, result){
        console.log(err);
    });
}

