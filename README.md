# Accedo Test Project

1. Application should display a list of videos in a horizontal carousel on the home page which can be scrollable.
2. User should be able to select a video to play in full screen.
3. When the video is finished, the application should go back to the previous page.
4. The user should be able to use a mouse and keyboard to select the video.
5. The user should be able to see a list of videos they have previously watched.
6. The application must be responsible to changing screen sizes. You do not need to implement a mobile view but it should at least adjust based on the desktop browser width.


# Prerequisites 
- The app should work in the latest version of Chrome Browser
- The app is suggested to be developed entirely in (X)HTML/JavaScript/CSS
- The viewing history should be saved on a server and persisted across browsing sessions. That is, if the user refreshes the browser the history should be automatically loaded. User authentication is not necessary.
- The server is suggested to be developed in either NodeJS, NoSQL, PHP, MySQL
- Data between the server and client side should be in JSON format. 
- Applicants are required to use JavaScript to complete the test. Detailed instructions about how to set up the app and server is required. A live demo hosted on a publically accessible server is highly regarded, for example AWS, Heroku, OpenShift.
- If you are using any cloud solutions to host or develop your application, credential(s) should be provided.
- The app should be controllable by the arrow keys (to navigate) and enter (to select), and mouse. 
- The app should follow the design template illustrated on the next page Suggested tools (but not required): 
    - Chrome Developer Tools (for debugging)
    - Frameworks such as JQuery, Express, Angular, Bootstrap, Sass/Less
    - Build tools such as Grunt, Gradle
    
    
# Runnning the Project
## To load the data in the database (mongodb)
`cd helpers`

`node load-data.js`

## To run the application
`npm install`

`cd public`

`bower install`

`cd ..`

`npm start`