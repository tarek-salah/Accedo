/**
 * Created by tarek on 9/10/15.
 */

$(document).ready(function () {
    $('.swiper-wrapper').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,


        arrows:true,
        variableWidth: true
    });


    //var swiper = new Swiper('.swiper-container', {
    //    slidesPerView: 4,
    //    centeredSlides: true,
    //    paginationClickable: true,
    //    spaceBetween: 30,
    //    nextButton: '.swiper-button-next',
    //    prevButton: '.swiper-button-prev',
    //    keyboardControl: true,
    //});
});