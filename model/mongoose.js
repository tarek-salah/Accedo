var mongoose = require('mongoose');

mongoose.connect(config.mongodb);


var UserSchema = new mongoose.Schema({
    email: { type: String, required: true, unique: true  },
    password: String,
    salt: String,
    hash: String,
    role:{ type: String, required: true },
    first_name:{ type: String, required: true },
    last_name:{ type: String, required: true },
});

var movieSchema = new mongoose.Schema({
    title: { type: String, required: true  },
    description: String,
    type: String,
    publishedDate: Date,
    availableDate: Date,
    metadata:[],
    contents:[],
    credits:[],
    parentalRatings:[],
    images:[],
    categories:[],
    id:{ type: String, required: true , unique: true }
});

exports.User = mongoose.model('users', UserSchema);
exports.Movie = mongoose.model('movies', movieSchema);