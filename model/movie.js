/**
 * Created by tarek on 10/17/15.
 */

var Movie = require('./mongoose').Movie;


exports.findOne = function (condition,callback) {
    Movie.findOne(condition , callback);
};


exports.listAll = function(callback){
    Movie.find({},'id title images', callback);
};

