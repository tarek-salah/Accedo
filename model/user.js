/**
 * Created by tarek on 9/8/15.
 */

var user = require('./mongoose').User;

exports.insert = function (data,callback){
    var user_entry = new user(data);
    user_entry.save(callback);
};

exports.findOne = function (condition,callback) {
    user.findOne(condition , callback);
};


exports.listAll = function(callback){
    user.find({},'-salt -hash', callback);
};

exports.count = function(condition, callback){
    user.count(condition, callback);
};

exports.update = function(condition, data,callback){
    user.findOneAndUpdate(
        condition , { $set: data },{new:true}, callback
    );
};

exports.delete = function(condition,callback){
    user.remove(condition, callback);
};
